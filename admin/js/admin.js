let sistema = new JOII.Sistema();
let id_seminario_actual = 0;
let id_visita_actual = 0;


function agregarEvento() {
    $('.lista-item-selected').removeClass('lista-item-selected')
    $('#lista_eventos').append('<span id="evento-1" class="lista-item lista-item-selected" onclick="seleccionarEvento()"> Evento 2 </span>')
}

function editarEvento() {
    let datos = $('.evento_dato');
    let boton = $('#boton_editar');

    if ( boton.html() == " Editar " ) {
        Array.prototype.forEach.call(datos, function(dato) {
            dato.contentEditable = "true"
        });

        boton.html(" Guardar ");
    }
    else {
        Array.prototype.forEach.call(datos, function(dato) {
            dato.contentEditable = "false"
        });

        boton.html(" Editar ");

        if ($('#tipo_evento option:selected').attr('value') == 1)
            sistema.modificarSeminario(generarSeminario());
        else
            sistema.modificarVisita(generarVisita());
    }
}

function generarSeminario() {
    return {
        id: id_seminario_actual,
        nombre: $('#nombre').html(),
        contacto: $('#contacto').html(),
        descripcion: $('#descripcion').html(),
        horario: new Date("2018", $('#mes').html(), $('#dia').html(), $('#hora').html(), $('#minutos').html() ),
        estado: $('#estado :selected').attr('value'),
        lugar: $('#lugar').html(),
        cupo: parseInt($('#cupo').html()),
        orador: $('#orador').html(),
        nombreConfirmado: $('#nombre_confirmado :selected').attr('value'),
    };
}

function generarVisita() {
    return {
        id: id_visita_actual,
        nombre: $('#nombre').html(),
        contacto: $('#contacto').html(),
        descripcion: $('#descripcion').html(),
        horario: new Date("2018", $('#mes').html(), $('#dia').html(), $('#hora').html(), $('#minutos').html() ),
        estado: $('#estado :selected').attr('value'),
        lugar: $('#lugar').html(),
        cupo: parseInt($('#cupo').html()),
        encargado: $('#encargado').html(),
        comentario: $('#comentario').html()
    };
}

function redirect() {
    window.location.href="../";
}