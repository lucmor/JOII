export class Sponsor {
	constructor(nombre, presentoCarpeta, interesado,
		categoria, pago, contacto) {
		this._nombre = nombre;
		this._presentoCarpeta = presentoCarpeta;
		this._interesado = interesado;
		this._categoria = categoria;
		this._pago = pago;
		this._contacto = contacto;
	}

	get nombre() {
		return this._nombre;
	}

	get presentoCarpeta() {
		return this._presentoCarpeta;
	}

	get interesado() {
		return this._interesado;
	}

	get categoria() {
		return this._categoria;
	}

	get pago() {
		return this._pago;
	}

	get contacto() {
		return this._contacto;
	}	
}