import { EventoImp, EventoObject } from './eventoImp'

export class Visita extends EventoImp {
	constructor(visitaObjeto) {
		super(visitaObjeto);

		this._encargado = visitaObjeto.encargado || null;
		this._comentario = visitaObjeto.comentario || "";
	}

    cargarObjeto(visita) {
        super.cargarObjeto(visita);

        this._encargado = visita.encargado;
        this._comentario = visita.comentario;
    }

	get encargado() {
		return this._encargado;
	}

	get comentario() {
		return this._comentario;
	}

	set encargado(encargado) {
		this._encargado = encargado;
	}

	set comentario(comentario) {
		this._comentario = comentario;
    }

    aObjeto() {
        let visitaObjeto = super.aObjeto();

        visitaObjeto.encargado = this.encargado;
        visitaObjeto.comentario = this.comentario;

        return visitaObjeto;
    }
}
