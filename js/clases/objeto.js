export class Objeto {
    /**
     * Obtiene el id del objeto
     * @return int Id del objeto
     */
    getId() {};

    /**
     * Establece el id del objeto
     * @param id Id nuevo de objeto
     */
    setId(id) {};

    /**
     * Cargar el objeto pasado por parametro en el objeto
     * @param objeto Objeto a cargar
     */
    cargarObjeto(objeto) {};

    /**
     * Devuelve un objeto con todos los atributos de la clase
     * @param objeto Objeto con los atributos de la clase
     * @return Objeto con los atributos de la clase
     */
    aObjeto(objeto) {};
}