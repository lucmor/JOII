import { Evento } from "./evento";
import { PersonaImp } from "./personaImp";

export class EventoImp extends Evento {
    constructor(eventoObjeto) {
        super();

        this._id = eventoObjeto.id || -1;
        this._nombre = eventoObjeto.nombre || "";
        this._contacto = eventoObjeto.contacto || new PersonaImp({});
        this._descripcion = eventoObjeto.descripcion || "";
        this._horario = eventoObjeto.horario || null;
        this._estado = eventoObjeto.estado || 0;
        this._cupo = eventoObjeto.cupo || 0;
        this._lugar = eventoObjeto.lugar || "";
    }

    cargarObjeto(evento) {
        this._nombre = evento.nombre;
        this._contacto = evento.contacto;
        this._descripcion = evento.descripcion;
        this._horario = evento.horario;
        this._estado = evento.estado;
        this._cupo = evento.cupo;
        this._lugar = evento.lugar;
    }

    get id(){
        return this._id;
    }

    get nombre() {
        return this._nombre;
    }

    get contacto() {
        return this._contacto;
    }

    get descripcion() {
        return this._descripcion;
    }

    get horario() {
        return this._horario;
    }

    get estado() {
        return this._estado;
    }

    get cupo() {
        return this._cupo;
    }

    get lugar() {
        return this._lugar;
    }

    set id(id) {
        this._id = id;
    }

    set nombre(nombre) {
        this._nombre = nombre;
    }

    set contacto(contacto) {
        this._contacto = contacto;
    }

    set descripcion(descripcion) {
        this._descripcion = descripcion;
    }

    set horario(horario) {
        this._horario = horario;
    }

    set estado(estado) {
        this._estado = estado;
    }

    set cupo(cupo) {
        this._cupo = cupo;
    }

    set lugar(lugar) {
        this._lugar = lugar;
    }

    aObjeto() {
        return {
            id: this.id,
            nombre: this.nombre,
            contacto: this.contacto,
            descripcion: this.descripcion,
            horario: this.horario,
            estado: this.estado,
            cupo: this.cupo,
            lugar: this.lugar
        };
    }
}