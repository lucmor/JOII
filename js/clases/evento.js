import { Objeto } from "./objeto";

export class Evento extends Objeto{
    getNombre() {};
    getContacto() {};
    getDescripcion() {};
    getHorario() {};
    getEstado() {};
    getCupo() {};
    getLugar() {};
    setNombre(nombre) {};
    setContacto(contacto) {};
    setDescripcion(descripcion) {};
    setHorario(horario) {};
    setEstado(estado) {};
    setCupo(cupo) {};
    setLugar(lugar) {};
}