import { EventoImp } from "./eventoImp";
import { PersonaImp } from "./personaImp";

export class Seminario extends EventoImp {
	constructor(seminarioObjeto) {
		super(seminarioObjeto);

		this._orador = seminarioObjeto.orador || new PersonaImp();
		this._nombreConfirmado = seminarioObjeto.nombreConfirmado || false;
    }

    cargarObjeto(seminario) {
        this._orador = seminario.orador;
        this._nombreConfirmado = seminario.nombreConfirmado;
    }

	get orador() {
		return this._orador;
	}

	get nombreConfirmado() {
		return this._nombreConfirmado;
	}

	set orador(orador) {
		this._orador = orador;
	}

	set nombreConfirmado(nombreConfirmado) {
		this._nombreConfirmado = nombreConfirmado;
    }

    aObjeto() {
        let seminarioObjeto = super.aObjeto();

        seminarioObjeto.orador = this.orador;
        seminarioObjeto.nombreConfirmado = this.nombreConfirmado;

        return seminarioObjeto;
    }
}
