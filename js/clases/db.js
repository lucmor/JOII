export class DB {
	obtenerInscriptos() {};
    obtenerSeminarios() {};
    obtenerVisitas() {};
    crearInscripto(inscripto) {};
    crearSeminario(seminario) {};
    crearVisita(visita) {};
    modificarInscripto(inscripto) {};
    modificarSeminario(seminario) {};
    modificarVisita(visita) {};
    eliminarInscripto(id) {};
    eliminarSeminario(id) {};
    eliminarVisita(id) {};
}