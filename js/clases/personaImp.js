import { Persona } from './persona'

export class PersonaImp extends Persona{
    constructor(personaObjeto) {
        super(personaObjeto);

        this._id = personaObjeto.id || 0;
        this._nombre = personaObjeto.nombre || "";
        this._apellido = personaObjeto.apellido || "";
        this._email = personaObjeto.email || "";
        this._celular = personaObjeto.celular || 0;
    }

    cargarObjeto(persona) {
        this._id = persona.id;
        this._nombre = persona.nombre;
        this._apellido = persona.apellido;
        this._email = persona.email;
        this._celular = persona.celular;
    }

    get id() {
        return this._id;
    }

    get nombre() {
        return this._nombre;
    }

    get apellido() {
        return this._apellido;
    }

    get email() {
        return this._email;
    }

    get celular() {
        return this._celular;
    }

    set id(id) {
        this._id = id;
    }

    set nombre(nombre) {
        this._nombre = nombre;
    }

    set apellido(apellido) {
        this._apellido = apellido;
    }

    set email(email) {
        this._email = email;
    }

    set celular(celular) {
        this._celular = celular;
    }

    aObjeto() {
        return {
            id: this.id,
            nombre : this.nombre,
            apellido: this.apellido,
            email: this.email,
            celular: this.celular
        };
    }
}