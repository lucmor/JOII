import { PersonaObject } from './personaImp';
import { Visita } from './visita';
import { PersonaImp } from "./personaImp";

export class Inscripto extends PersonaImp {
	constructor(inscriptoObjeto) {
		super(inscriptoObjeto);

        this._universidad = inscriptoObjeto.universidad || 0;
		this._lu = inscriptoObjeto.lu || 0;
		this._carrera = inscriptoObjeto.carrera || 0;
		this._dni = inscriptoObjeto.dni || 0;
		this._fechaNac = new Date(inscriptoObjeto.fechaNac);
        this._seminarios = inscriptoObjeto.seminarios || [];
		this._visita = inscriptoObjeto.visita || new Visita({});
		this._vaFiesta = inscriptoObjeto.vaFiesta || false;
		this._esCeliaco = inscriptoObjeto.esCeliaco || false;
		this._esVegetariano = inscriptoObjeto.esVegetariano || false;
	}

    cargarObjeto(inscripto) {
        super.cargarObjeto(inscripto);
    
        this._universidad = inscripto.universidad;
        this._lu = inscripto.lu;
        this._carrera = inscripto.carrera;
        this._dni = inscripto.dni;
        this._fechaNac = inscripto.fechaNac;
        this._seminarios = inscripto.seminarios;
        this._visita = inscripto.visita;
        this._vaFiesta = inscripto.vaFiesta;
        this._esCeliaco = inscripto.esCeliaco;
        this._esVegetariano = inscripto.esVegetariano;
    }

    get universidad() {
        return this._universidad;
    }

	get lu() {
		return this._lu;
	}

	get carrera() {
		return this._carrera;
	}

	get dni() {
		return this._dni;
	}

	get fechaNac() {
		return this._fechaNac;
	}

    get seminarios() {
		return this._seminarios;
	}

	get visita() {
		return this._visita;
	}

	get vaFiesta() {
		return this._vaFiesta;
	}

	get esCeliaco() {
		return this._esCeliaco;
	}

	get esVegetariano() {
		return this._esVegetariano;
	}

	set universidad(universidad) {
		this._universidad = universidad;
	}

	set lu(lu) {
		this._lu = lu;
	}

	set carrera(carrera) {
		this._carrera = carrera;
    }

	set dni(dni) {
		this._dni = dni;
	}

	set fechaNac(fechaNac) {
		this._fechaNac = fechaNac;
	}

	agregarSeminario(seminario) {
		this.seminarios.push(seminario);
	}

    set visita(visita) {
		this._visita = visita;
	}

	set vaFiesta(va) {
		this._vaFiesta = va;
	}

	set esCeliaco(es) {
		this._esCeliaco = es;
	}

	set esVegetariano(es) {
		this._esVegetariano = es;
	}

    aObjeto() {
    	let inscriptoObject = super.aObjeto();

        inscriptoObject.universidad = this.universidad;
        inscriptoObject.lu = this.lu;
        inscriptoObject.carrera = this.carrera;
        inscriptoObject.dni = this.dni;
        inscriptoObject.fechaNac = this.fechaNac.toDateString();
        inscriptoObject.seminarios = this.seminarios;
        inscriptoObject.visita = this.visita;
        inscriptoObject.vaFiesta = this.vaFiesta;
        inscriptoObject.esCeliaco = this.esCeliaco;
        inscriptoObject.esVegetariano = this.esVegetariano;
        
        return inscriptoObject;
	}
}
