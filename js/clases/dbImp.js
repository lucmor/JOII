import Dexie from "../libs/Dexie";
import { DB } from "./db";

const DB_NAME = "JOIIDB";
const DB_VERSION = 1;

export class DBImp extends DB {
    constructor() {
        super();
        this.db = new Dexie(DB_NAME);

        this.db.version(DB_VERSION).stores({
            inscriptos: '++id',
            visitas: '++id',
            seminarios: '++id'
        });
    }

    get inscriptos() {
        return this.db.table("inscriptos").toCollection();
    }

    get visitas() {
        return this.db.table("visitas").toCollection();
    }

    get seminarios() {
        return this.db.table("seminarios").toCollection();
    }

    crearInscripto(inscripto) {
        return this.db.table("inscriptos").put(inscripto.aObjeto()).then(() => {
            let inscripto_data = this.db.table("inscriptos");

            return inscripto_data.orderBy("id").last(function(item) {
                inscripto.id = item.id;

                return item.id;
            });
        });
    }

    crearVisita(visita) {
        return this.db.table("visitas").put(visita.aObjeto()).then(() => {
            let visita_data = this.db.table("visitas");

            return visita_data.orderBy("id").last(function(item) {
                visita.id = item.id;

                return item.id;
            });
        });
    }

    crearSeminario(seminario) {
        return this.db.table("seminarios").put(seminario.aObjeto()).then(() => {
            let seminario_data = this.db.table("seminarios");

            return seminario_data.orderBy("id").last(function(item) {
                seminario.id = item.id;

                return item.id;
            });
        });
    }

    modificarInscripto(inscripto) {
        return this.db.table("inscriptos").update(inscripto.id, inscripto.aObjeto());
    }

    modificarSeminario(seminario) {
        return this.db.table("seminarios").update(seminario.id, seminario.aObjeto());
    }

    modificarVisita(visita) {
        return this.db.table("visista").update(visita.id, visita.aObjeto());
    }

    eliminarInscripto(id) {
        return this.db.table("inscriptos").delete(id);
    }

    eliminarSeminario(id) {
        return this.db.table("seminarios").delete(id);
    }

    eliminarVisita(id) {
        return this.db.table("visita").delete(id);
    }
}