const sistema = new JOII.Sistema();
sistema.cargarDatosParaGUI().then(() => {
    sistema.inscriptos.forEach(function(inscripto, id, map) {
        $("#lista_inscriptos").append('<span id="inscripto' + id + '" class="lista-item lista-item-selected" onclick="seleccionarInscripto(event)">' +
            inscripto.nombre + ' ' + inscripto.apellido + '</span>');
    });
});

let id_actual = 0;


function editarInscripto() {
    let datos = $(".alumno_dato");
    let boton_editar = $("#boton_editar");


    if( boton_editar.html() == " Editar ") {
        Array.prototype.forEach.call(datos, function(dato) {
            dato.contentEditable = "true";
        });

        boton_editar.html(" Guardar ");
    }
    else { 
        Array.prototype.forEach.call(datos, function(dato) {
            dato.contentEditable = "false";
        });

        boton_editar.html(" Editar ");

        sistema.modificarInscripto(id_actual, generarInscripto()).then(function() {
            let inscripto = sistema.obtenerInscripto(id_actual);
            $('#inscripto' + id_actual).text(inscripto.nombre + ' ' + inscripto.apellido);
        });
    }
    
}

function eliminarInscripto() {
    sistema.eliminarInscripto(id_actual);
}

function agregarInscripto() {
    $('.lista-item-selected').removeClass('lista-item-selected');
    $('#lista_inscriptos').append('<span id="inscripto-1" class="lista-item lista-item-selected" onclick="seleccionarInscripto(event)">   </span>');
    

    $('#nombre').html('');
    $('#apellido').html('');
    $('#email').html('');
    $('#celular').html('');
    $('#universidad').attr('selectedIndex', 0);
    $('#lu').html('');
    $('#carrera').attr('selectedIndex', 0);
    $('#dni').html('');
    $('#anio_nacimiento').html('1970');
    $('#mes_nacimiento').html('01');
    $('#dia_nacimiento').html('01');
    $('#seminarios').attr('selectedOptions', []);
    $('#visita').attr('selectedIndex', 0);
    $('#fiesta').attr('checked', false);
    $('#celiaco').attr('checked', false);
    $('#vegetariano').attr('checked', false);

    sistema.agregarInscripto().then(function(id) {
        $('#inscripto-1').attr('id', 'inscripto'+id);
    });
}

function generarInscripto() {
    let seminarios = [];

    $('#seminarios :selected').each(function() {
        seminarios.push($(this).val());
    });

    return {
        id: id_actual,
        nombre: $('#nombre').html(),
        apellido: $('#apellido').html(),
        email: $('#email').html(),
        celular: $('#celular').html(),
        universidad: $('#universidad :selected')[0].value,
        lu: $('#lu').html(),
        carrera: $('#carrera :selected')[0].value,
        dni: $('#dni').html(),
        fechaNac: new Date($('#anio_nacimiento').text(), $('#mes_nacimiento').text(), $('#dia_nacimiento').text()),
        seminarios: seminarios,
        visita: sistema.obtenerVisita($('#visita :selected')[0].value),
        fiesta: $('#fiesta').is(':checked'),
        celiaco: $('#celiaco').is(':checked'), 
        vegetariano: $('#vegetariano').is(':checked')
    }
}


function seleccionarInscripto(event) {
    id_actual = parseInt(event.target.id.substr(9));

    $('.lista-item-selected').removeClass('lista-item-selected');
    $('.right-side').removeClass('not-visible');
    event.target.classList.add('lista-item-selected');
    
    var inscripto = sistema.obtenerInscripto(id_actual);

    $('#nombre').html(inscripto.nombre);
    $('#apellido').html(inscripto.apellido);
    $('#email').html(inscripto.email);
    $('#celular').html(inscripto.celular);
    $('#universidad').attr('selectedIndex', inscripto.universidad);
    $('#lu').html(inscripto.lu);
    $('#carrera').attr('selectedIndex', inscripto.carrera);
    $('#dni').html(inscripto.dni);
    $('#anio_nacimiento').html(inscripto.fechaNac.getFullYear());
    $('#mes_nacimiento').html(inscripto.fechaNac.getMonth() + 1);
    $('#dia_nacimiento').html(inscripto.fechaNac.getDate());
    $('#seminarios').attr('selectedOptions', inscripto.seminarios.map(seminario => seminario.id));
    $('#visita').attr('selectedIndex', inscripto.visita.id);
    $('#fiesta').attr('checked', inscripto.vaFiesta);
    $('#celiaco').attr('checked', inscripto.esCeliaco);
    $('#vegetariano').attr('checked', inscripto.esVegetariano);
}

function redirect() {
    window.location.href="admin/";
}
