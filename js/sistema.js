import { Inscripto, InscriptoObject } from "./clases/inscripto";
import { Seminario, SeminarioObject } from "./clases/seminario";
import { Visita, VisitaObject } from "./clases/visita";
import { DBImp } from "./clases/dbImp";

export class Sistema {
    constructor() {
        this._inscriptos = new Map();
        this._seminarios = new Map();
        this._visitas = new Map();
        this._db = new DBImp();

        this.sistemaCargado = false;

        this.cargadb = this.cargarDesdeDB();
    }

    cargarDesdeDB() {
        return this._db.inscriptos.each((inscripto) => {
            let nuevoInscripto = new Inscripto(inscripto);

            this._inscriptos.set(nuevoInscripto.id, nuevoInscripto);
        }).then(() => {

            this._db.visitas.each((visita) => {
                let nuevaVisita = new Visita(visita);

                this._visitas.set(nuevaVisita.id, nuevaVisita);
            });
        }).then(() => {
            this._db.seminarios.each((seminario) => {
                let nuevoSeminario = new Seminario(seminario);

                this._seminarios.set(nuevoSeminario.id, nuevoSeminario);
            });
        }).then(() => {
            this.sistemaCargado = true;
        });
    }

    cargarDatosParaGUI() {
        return this.cargadb;
    }

    get inscriptos() {
        return this._inscriptos;
    }

    get seminarios() {
        return this._seminarios;
    }

    get visitas() {
        return this._visitas;
    }

    agregarInscripto() {
        let inscriptoNuevo = new Inscripto({});

        return this._db.crearInscripto(inscriptoNuevo).then((id) => {
            this._inscriptos.set(id, inscriptoNuevo);

            return id;
        });
    }

    agregarSeminario() {
        let seminarioNuevo = new Seminario({});

        return this._db.crearSeminario(seminarioNuevo).then((id) => {
            this._seminarios.set(id, seminarioNuevo);

            return id;
        });
    }

    agregarVisita() {
        let visitaNueva = new Visita({});

        return this._db.crearVisita(visitaNueva).then((id) => {
            this._visitas.set(id, visitaNueva);

            return id;
        });
    }

    obtenerInscripto(id) {
        return this._inscriptos.get(id);
    }

    obtenerSeminario(id) {
        return this._seminarios.get(id);
    }

    obtenerVisita(id) {
        return this._visitas.get(id);
    }

    modificarInscripto(id, datos) {
        console.log(datos.fechaNac);
        this._inscriptos.get(id).cargarObjeto(datos);
        console.log(this._inscriptos.get(id).fechaNac);

        return this._db.modificarInscripto(this._inscriptos.get(id));
    }

    modificarSeminario(id, datos) {
        this._seminarios.get(id).cargarObjeto(datos);

        this._db.modificarSeminario(this._seminarios.get(id));
    }

    modificarVisita(id, datos) {
        this._visitas.get(id).cargarObjeto(datos);

        this._db.modificarVisita(this._visitas.get(id));
    }

    eliminarInscripto(id) {
        this._visitas.delete(id);

        this._db.eliminarInscripto(id);
    }

    eliminarSeminario(id) {
        this._seminarios.delete(id);

        this._db.eliminarSeminario(id);
    }

    eliminarVisita(id) {
        this._visitas.delete(id);

        this._db.eliminarVisita(id);
    }
}
