const path = require('path');

module.exports = {
    entry: './js/sistema.js',
    module: {
        rules: [
            {
                test: /\.js?$/,
                use: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    },
    mode: 'development',
    resolve: {
        extensions: ['.js']
    },
    output: {
        filename: 'sistema.bundle.js',
        path: path.resolve(__dirname, 'js/'),
        library: 'JOII',
        libraryTarget: 'var'
    }
}
